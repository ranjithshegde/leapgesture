#include "gestureRecognition.hpp"

//! This class contains a set of tools to calculate motion derivatives for values of type `Template T`
class GestureRecognition::Gesture {
public:
    Gesture(isGesture properties);
    Gesture();
    ~Gesture();

    void setup(isGesture properties);

    //! A simple data structure to contain all relevant derivative values
    //! `GestureRecognition::Gesture::derivatives::firstDt` and `GestureRecognition::Gesture::derivatives::secondDt` store the velocity and acceleration respectively
    //! `GestureRecognition::Gesture::derivatives::oldFirst` and `GestureRecognition::Gesture::derivatives::oldSecond` store the previous values for velocity and acceleration respectively
    //! `GestureRecognition::Gesture::derivatives::period` sets the measurement period for average calculation
    //! `GestureRecognition::Gesture::derivatives::averageDt` and `GestureRecognition::Gesture::derivatives::averageSecondDt` store the average velocity and acceleration respectively
    struct derivatives {
        //! Current velocity
        float firstDt;
        //! Current acceleration
        float secondDt;
        //! Previous velocity
        float oldFirst;
        //! Previous acceleration
        float oldSecond;
        //! Duration to meaure averages
        unsigned int period;
        //! Current index in the duration cycle
        unsigned int count;
        //! Average velocity
        double averageDt;
        //! Average acceleration
        double averageSecondDt;

        //! Default initializer
        derivatives()
        {
        }

        //! Skip initialization to a specific value
        derivatives(int value)
        {
            firstDt = value;
            secondDt = value;
            oldFirst = value;
            oldSecond = value;
            period = value;
            count = value;
            averageDt = value;
            averageSecondDt = value;
        }

        //! Copy constructor
        void operator()(int value)
        {
            firstDt = value;
            secondDt = value;
            oldFirst = value;
            oldSecond = value;
            period = value;
            count = value;
            averageDt = value;
            averageSecondDt = value;
        }
    };

    template <typename T>
    //! Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop.
    //! It stores the previous value and compares it against the current value
    void derive(T value);
    //! Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop.
    //! It stores the previous value and compares it against the current value
    void derive();

    //! Calculates and stores the average for the value set in `GestureRecognition::Gesture::derive`.
    //! The time period for measurement is set from `GestureRecognition::Gesture::m_Deriv.period` which can be set from `GestureRecognition::Gesture::setPeriod`
    void average();

    //! Returns the current velocity calculated from the most recent `GestureRecognition::Gesture::derive` call.
    float velocity();
    //! Returns the current acceleartion calculated from the most recent `GestureRecognition::Gesture::derive` call.
    float acceleartion();
    //! Returns the previous velocity calculated from the most recent `GestureRecognition::Gesture::derive` call.
    float oldValue();
    //! Returns the previous acceleration calculated from the most recent `GestureRecognition::Gesture::derive` call.
    float oldDt();

    //! Sets the period for the calculation of average value by `GestureRecognition::Gesture::average`
    inline void setPeriod(unsigned int period)
    {
        m_Deriv.period = period;
    }

    //! Returns the average velocity calculated by `GestureRecognition::Gesture::average`
    double avgVelocity();
    //! Returns the average acceleartion calculated by `GestureRecognition::Gesture::average`
    double avgAccel();

private:
    derivatives m_Deriv;
    double tempFirst, tempSecond;
    GestureRecognition::isGesture m_Properties;
    GestureRecognition::gType m_Type;
};
