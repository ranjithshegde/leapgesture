#include "Identifier.hpp"

GestureRecognition::Identifier::Identifier(GestureRecognition::Device dType)
    : m_Device(dType)
{
}

GestureRecognition::Identifier::Identifier()
{
}

void GestureRecognition::Identifier::setDevice(GestureRecognition::Device dType)
{
    m_Device = dType;
}

bool GestureRecognition::Identifier::isPointed(handData data)
{
    bool p = false;
    int pointed[2] = { 0, 0 };
    fingerType fingerTypes[] = { THUMB, INDEX, MIDDLE, RING, PINKY };
    for (auto d : data) {
        for (int i = 0; i < d.size(); i++) {
            if (d[i].fingers()[INDEX].isExtended()) {
                pointed[i]++;
            }
            // d[i].fingers()[INDEX]
            // if (hand.fingers()[fingerTypes[4]].tipPosition().x < hand.fingers()[fingerTypes[0]].tipPosition().x) {
            // }
        }
    }
    for (size_t i = 0; i < 2; i++) {
        if (pointed[i] > 50) {
            p = true;
            m_FingerLoc[i].x = data[data.size() - 1][i].fingers()[fingerTypes[INDEX]].tipPosition().x;
            m_FingerLoc[i].y = data[data.size() - 1][i].fingers()[fingerTypes[INDEX]].tipPosition().y;
            m_FingerLoc[i].z = data[data.size() - 1][i].fingers()[fingerTypes[INDEX]].tipPosition().z;
        }
    }
    return p;
}

bool GestureRecognition::Identifier::isRotation(handData data)
{
    bool p = false;
    glm::vec2 centroid;
    std::vector<float> radii;
    vector<glm::vec2> points;
    fingerType fingerTypes[] = { THUMB, INDEX, MIDDLE, RING, PINKY };
    for (auto d : data) {
        if (d.size()) {
            glm::vec2 temp = { d[0].fingers()[fingerTypes[1]].tipPosition().x, d[0].fingers()[fingerTypes[1]].tipPosition().y };
            points.push_back(temp);
        }
    }
    centroid = GestureRecognition::getCentroid(points);
    for (size_t i = 0; i < points.size(); i++) {
        // float radius = glm::length(glm::distance(centroid, points[i]));
        // radii.push_back(radius);

        radii.push_back(glm::length(glm::distance(centroid, points[i])));
    }

    float dev = GestureRecognition::calculateSD(radii);
    if (dev > 5 && dev < 20) {
        p = true;
    }
    return p;
}

bool GestureRecognition::Identifier::isClap(handData data)
{
    fingerType fingerTypes[] = { THUMB, INDEX, MIDDLE, RING, PINKY };
    bool c = false;
    float dist = 0;
    for (auto d : data) {
        if (d.size() > 1) {
            for (size_t i = 0; i < 5; i++) {
                // clang-format off
                if (
                    (d[0].fingers()[fingerTypes[i]].tipPosition().x - d[1].fingers()[fingerTypes[i]].tipPosition().x) < 1 &&
                    (d[0].fingers()[fingerTypes[i]].tipPosition().y - d[1].fingers()[fingerTypes[i]].tipPosition().y) < 1 && 
                    (d[0].fingers()[fingerTypes[i]].tipPosition().z - d[1].fingers()[fingerTypes[i]].tipPosition().z) < 1)
                dist++;
                // clang-format on
            }
        }
    }
    if (dist > 20) {
        c = true;
    }
    return c;
}

GestureRecognition::gType GestureRecognition::Identifier::find(handData& data)
{
    if (isRotation(data)) {
        m_IsGesture.type = GestureRecognition::SWIRL;
    } else if (isClap(data)) {
        m_IsGesture.type = GestureRecognition::CLAP;
    } else if (isPointed(data)) {
        m_IsGesture.type = GestureRecognition::POINT;
    } else {
        m_IsGesture.type = GestureRecognition::NONE;
    }

    return m_IsGesture.type;
}
