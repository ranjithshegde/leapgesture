#include "ofApp.h"
#include "ofGraphics.h"
#define FINGERS(x, y, z) hands[x].fingers()[y[z]]
//--------------------------------------------------------------

void ofApp::setup()
{
    ident.setDevice(dev);
    ofSetFrameRate(60);
    ofSetVerticalSync(true);

    leap.open();

    // keep app receiving data from leap motion even when it's in the background
    leap.setReceiveBackgroundFrames(true);

    cam.setOrientation(ofPoint(-20, 0, 0));

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

    watch.start();
}

//--------------------------------------------------------------
void ofApp::update()
{
    // fingersFound.clear();

    hands = leap.getLeapHands();
    if (leap.isFrameNew() && hands.size()) {

        //     // you can get back a mapped point by using ofxLeapMotion::getMappedofPoint with the Leap::Vector that tipPosition returns
        leap.setMappingX(-230, 230, -ofGetWidth() / 2.f, ofGetWidth() / 2.f);
        leap.setMappingY(90, 490, -ofGetHeight() / 2.f, ofGetHeight() / 2.f);
        leap.setMappingZ(-150, 150, -200, 200);

        // fingerType fingerTypes[] = { THUMB, INDEX, MIDDLE, RING, PINKY };

        // for (int i = 0; i < hands.size(); i++) {
        // fingers.handPos = leap.getMappedofPoint(hands[i].palmPosition());
        // for (int f = 0; f < 5; f++) {
        // const Finger& finger = hands[i].fingers()[fingerTypes[f]];

        // fingers.dip = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_DIP));
        // fingers.mcp = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_MCP));
        // fingers.pip = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_PIP));
        // fingers.tip = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_TIP));
        //             ofPoint pt;

        //             const Finger& finger = hands[i].fingers()[fingerTypes[j]];

        //             // here we convert the Leap point to an ofPoint - with mapping of coordinates
        //             // if you just want the raw point - use ofxLeapMotion::getofPoint
        //             pt = leap.getMappedofPoint(finger.tipPosition());
        //             pt = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_DIP));

        //             fingersFound.push_back(finger.id());
        // }
        // if (hands[i].fingers()[fingerTypes[4]].tipPosition().x < hands[i].fingers()[fingerTypes[0]].tipPosition().x  ) {
        //     Hands.leftHand = fingers;
        // }
        // else
        // {
        //     Hands.rightHand = fingers;
        // }
        // }
    }

    analyzer.push_back(hands);
    leap.markFrameAsOld();
}

//--------------------------------------------------------------
//! Here is the reference implementation. Small balls (filled circles) are drawn on screen.
//! Their location is controlled by the kind of identified gesture. They move ot different quadrants
//! of the screen when the gesture changes.\n
//! A straight line is drawn based on the relationship between the left & right hand. Duration of their close proximity forms the coordinats of the start,
//! The distance between the hands forms the xCoord of the end, with the magnitue of the vetor formed by the angle between both thumbs forms the yCoord of the end.
//! Another circle is drawn based on the relationship formed between left thumb and right wrist
void ofApp::draw()
{
    if (hands.size()) {
        if ((ofGetFrameNum() * 2) % 120 == 0) {
            type = ident.find(analyzer);
            analyzer.clear();
            cout << type << endl;
        }
    }

    if (type == 0) {
        radius = 20;
        ofDrawCircle(ofGetWidth() / 2.f, ofGetHeight() / 2.f, radius);
    } else if (type == 1) {
        radius = 40;
        ofDrawCircle(ofGetWidth() / 3.f, ofGetHeight() / 3.f, radius);
    } else if (type == 2) {
        radius = 60;
        ofDrawCircle(ofGetWidth() / 4.f, ofGetHeight() / 4.f, radius);
    } else if (type == 4) {
        radius = 0;
    }
    ofBackgroundGradient(ofColor(0x005A5A5A), ofColor(30, 30, 30), OF_GRADIENT_BAR);

    ofSetColor(200);
    ofDrawBitmapString("ofxLeapMotion - Example App\nLeap Connected? " + ofToString(leap.isConnected()), 20, 20);

    cam.begin();

    ofPushMatrix();
    ofRotateDeg(90, 0, 0, 1);
    ofSetColor(20);
    ofDrawGridPlane(800, 20, false);
    ofPopMatrix();

    fingerType fingerTypes[] = { THUMB, INDEX, MIDDLE, RING, PINKY };

    if (hands.size() > 1) {

        glm::vec3 thumb = leap.getMappedofPoint(FINGERS(0, fingerTypes, 0).tipPosition());
        float thMag = glm::length(thumb);
        glm::vec3 wrist = leap.getMappedofPoint(hands[1].wristPosition());
        float wrMag = glm::length(wrist);
        glm::vec3 h0 = leap.getMappedofPoint(hands[0].palmPosition());
        float hMag = glm::length(h0);
        glm::vec3 h1 = leap.getMappedofPoint(hands[1].palmPosition());
        float HMag = glm::length(h1);

        float distance = glm::distance(hMag, HMag);

        if (distance < 65) {
            watch.restart();
            counter++;
            float time = ofMap(watch.elapsed(), 1, 50000, 30, ofGetHeight(), true);
            ofDrawLine(time, HMag, (distance + 1) * 20, hMag);
        } else {
            //     //         // } else if (distance > 45) {
            //     //         // if (counter) {
            //     //         // cout << watch.elapsed() <<"  " << distance <<  endl;
            //     //         // }
            watch.stop();
        }
        glm::vec2 trialPoint(thMag, wrMag);
        ofDrawCircle(trialPoint, 20);
    }

    for (int i = 0; i < hands.size(); i++) {
        glm::vec3 handPos = leap.getMappedofPoint(hands[i].palmPosition());
        glm::vec3 handNormal = leap.getMappedofPoint(hands[i].palmNormal());

        ofSetColor(0, 0, 255);
        ofDrawSphere(handPos.x, handPos.y, handPos.z, 20);
        ofSetColor(255, 255, 0);
        ofDrawArrow(handPos, handPos + 100 * handNormal);

        for (int f = 0; f < 5; f++) {

            const Finger& finger = hands[i].fingers()[fingerTypes[f]];

            glm::vec3 dip = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_DIP));
            glm::vec3 mcp = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_MCP));
            glm::vec3 pip = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_PIP));
            glm::vec3 tip = leap.getMappedofPoint(finger.jointPosition(finger.JOINT_TIP));

            ofSetColor(0, 255, 0);
            ofDrawSphere(mcp.x, mcp.y, mcp.z, 12);
            ofDrawSphere(pip.x, pip.y, pip.z, 12);
            ofDrawSphere(dip.x, dip.y, dip.z, 12);
            ofDrawSphere(tip.x, tip.y, tip.z, 12);

            ofSetColor(255, 0, 0);
            ofSetLineWidth(20);
            ofDrawLine(mcp.x, mcp.y, mcp.z, pip.x, pip.y, pip.z);
            ofDrawLine(pip.x, pip.y, pip.z, dip.x, dip.y, dip.z);
            ofDrawLine(dip.x, dip.y, dip.z, tip.x, tip.y, tip.z);
        }
    }
    cam.end();
    // Leap::ImageList images = leap.getImageList();
    // for(int i = 0; i < 2; i++){
    //     Leap::Image image = images[i];
    //     if (image.data()) {
    //         const unsigned char* image_buffer = image.data();
    //         ofImage leapImage;
    //         leapImage.setFromPixels(image_buffer, image.width(), image.height(), OF_IMAGE_GRAYSCALE);
    //         leapImage.draw(i * image.width(), image.height(), image.width(), image.height());
    //     }
    // }
}

//--------------------------------------------------------------
void ofApp::exit()
{
    leap.close();
}
