#pragma once
#include "ofMain.h"
#include "omp.h"

//! This is a global namespace that wraps several classes and subclasses relating to gesture recognition.
//! It wraps the classes based on input devices & gesture kernels
namespace GestureRecognition {
//! Identifiers for basic gesture kernels
enum gType {
    //! Circular or semi circular motion
    SWIRL,
    //! One or more erect fingers
    POINT,
    //! Two hands with flat position in close proximity
    CLAP,
    //! Hands separted in space with fingers stretching out
    SPREAD,
    //! No recognizable gesture
    NONE
};

//! Identifiers for gestuare capable devices
enum Device {
    //! Leap Motion hand tracker
    LEAP,
    //! Kinect V1 with tilt support
    KINECTV1,
    //! Kinect V2 with body tracker for upto six people
    KINECTV2,
    //! Mouse, trackpad or Wacom Tablet
    MOUSE,
    //! Any device recognized by `openCV`
    CV,
    //! Audio input specific to voice.
    VOICE
};

//! A basic data structure to record the current value & type of gesture
struct isGesture {
    //! The current co-ordinates to track gesture from
    glm::vec3 location;
    // The type of gesture as identified under `GestureRecognition::gType`
    gType type;
};

//! Return centre of mass for the specified vertices.
//! This is done by first forming a polygon sequentially from the vertices and then calculating its area.
//! The normalized value is returned as centre or mass.
glm::vec2 getCentroid(vector<glm::vec2> vertices);

//! Return the standard deviation of the data collection.
//! The mean value of the data set is calculated first.
//! Each value subtracted with the mean is squared and summed.
//! The normalized square-root of the sum is returned.
float calculateSD(std::vector<float> data);

//! A collection of gesture identification functions and algorithms
class Identifier;
//! A collection of functions to idenfity and manipulate certain properties of identified gestures.
class Gesture;
class Locator;
}
