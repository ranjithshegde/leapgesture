#pragma once

#include "Identifier.hpp"
#include "Poco/Stopwatch.h"
#include "gestureRecognition.hpp"
#include "ofMain.h"
#include "ofxLeapMotion2.h"

//! Data structure for joints of each finger
struct fingerStucture {
    glm::vec3 dip, mcp, pip, tip;
};

//! Data structure for a hand with individual fingers using
//! `ofApp::fingerStructure`
struct handStructure {
    glm::vec3 handPos;
    fingerStucture thumb, index, middle, ring, little;
};

//! Both hands together in a single structure
struct leapHands {
    handStructure leftHand, rightHand;
};

class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();

    //! Destroy Leap Motion data on exit
    void exit();

    //! Leap Motion API bindings
    ofxLeapMotion leap;
    // vector<ofxLeapMotionSimpleHand> simpleHands;
    //! Hand structure and releveant data as exposed by Leap Motion API
    vector<Hand> hands;

    vector<int> fingersFound;
    //! A mouse controllable camera Matrix
    ofEasyCam cam;
    float timer = 0.0;
    Poco::Stopwatch watch;
    int counter = 0;

    ofImage img;
    fingerStucture fingers;
    handStructure structure;
    leapHands Hands;

    vector<vector<Hand>> analyzer;
    GestureRecognition::Device dev = GestureRecognition::LEAP;
    GestureRecognition::Identifier ident;

    GestureRecognition::gType type;
    float radius = 0;
};
