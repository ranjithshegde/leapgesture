#include "gestureRecognition.hpp"
#include "ofxLeapMotion2.h"
typedef std::vector<std::vector<Leap::Hand>> handData;

//! This class accepts a device enum `GestureRecognition::Device` and parses its
//! output to query for gestures. The identified gestures are assiciated with
//! the appropriate enums from `GestureRecognition::gType`
class GestureRecognition::Identifier {
public:
    //! Default constructor
    Identifier(Device dType);
    Identifier();

    //! Set the device for its data be inspected. The value has to be of
    //! type `GestureRecognition::Device`
    void setDevice(Device dType);

    //! The starting point of the algorithm. Pass the relevant data sets from
    //! the device. It iterates through possible gestures and returns when it
    //! finds one. Returns `GestureRecognition::gTyoe NONE` when unsuccessful.
    gType find(handData& data);

    //! Returns true or false for pointing gesture.
    //! If all the joints of any single finger form a semi-straight line, it
    //! returns true
    bool isPointed(handData data);

    //! Returns true or false for rotation or swirling gesture.
    //! The dataset accumelated over a single second (60 frames, so 60 sets) are
    //! measured for center of mass using `GestureRecognition::getCentroid`
    //! which forms the center of the circle. The distance between the center
    //! and each vertex in the data set is measured and calculated for standard
    //! deviation using `GestureRecognition::calculateSD`. Returns true when the
    //! SD is within a desired range.
    bool isRotation(handData data);

    //! Returns true or false for clapping gesture.
    //! Checks the proximity of individual joints of each finger. When they are
    //! close it increments a counter. If the counter is larger than twenty, or
    //! in other words if more than twenty of the fourty six joints are close
    //! enough then it returns true.
    bool isClap(handData data);
    // bool isSpread(handData data);

private:
    Device m_Device;
    handData m_Data;
    ofxLeapMotion m_Leap;
    glm::vec3 m_FingerLoc[2];
    GestureRecognition::isGesture m_IsGesture;
    GestureRecognition::Gesture m_Gesture();
};
