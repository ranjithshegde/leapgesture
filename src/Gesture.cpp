#include "Gesture.hpp"

GestureRecognition::Gesture::Gesture()
{
}

GestureRecognition::Gesture::Gesture(isGesture properties)
    : m_Deriv(0)
    , m_Properties(properties)
{
    tempFirst = 0;
    tempSecond = 0;
}

void GestureRecognition::Gesture::setup(isGesture properties)
{
    m_Deriv(0);
    tempFirst = 0;
    tempSecond = 0;
    m_Properties = properties;
}

GestureRecognition::Gesture::~Gesture()
{
}

template <typename T>
void GestureRecognition::Gesture::derive(T value)
{
    m_Deriv.firstDt = value - m_Deriv.oldFirst;
    m_Deriv.secondDt = m_Deriv.firstDt - m_Deriv.oldSecond;
    m_Deriv.oldFirst = value;
    m_Deriv.oldSecond = m_Deriv.firstDt;
}

void GestureRecognition::Gesture::derive()
{
    float value = glm::length(m_Properties.location);
    m_Deriv.firstDt = value - m_Deriv.oldFirst;
    m_Deriv.secondDt = m_Deriv.firstDt - m_Deriv.oldSecond;
    m_Deriv.oldFirst = value;
    m_Deriv.oldSecond = m_Deriv.firstDt;
}

void GestureRecognition::Gesture::average()
{
    if (m_Deriv.count < m_Deriv.period) {
        tempFirst += m_Deriv.firstDt;
        tempSecond += m_Deriv.secondDt;
        m_Deriv.count++;
    } else {
        m_Deriv.averageDt = tempFirst;
        m_Deriv.averageSecondDt = tempSecond;
        m_Deriv.count = 0;
    }
}

float GestureRecognition::Gesture::velocity()
{
    return m_Deriv.firstDt;
}

float GestureRecognition::Gesture::acceleartion()
{
    return m_Deriv.secondDt;
}

float GestureRecognition::Gesture::oldValue()
{
    return m_Deriv.oldFirst;
}

float GestureRecognition::Gesture::oldDt()
{
    return m_Deriv.oldSecond;
}

double GestureRecognition::Gesture::avgAccel()
{
    return m_Deriv.averageSecondDt;
}

double GestureRecognition::Gesture::avgVelocity()
{
    return m_Deriv.averageDt;
}
