#include "gestureRecognition.hpp"

glm::vec2 GestureRecognition::getCentroid(vector<glm::vec2> vertices)
{
    glm::vec2 centroid = { 0, 0 };

    double signedArea = 0.0;
    for (int i = 0; i < vertices.size(); ++i) {
        double x0 = vertices[i].x;
        double y0 = vertices[i].y;
        double x1 = vertices[(i + 1) % vertices.size()].x;
        double y1 = vertices[(i + 1) % vertices.size()].y;
        double a = x0 * y1 - x1 * y0;
        signedArea += a;
        centroid.x += (x0 + x1) * a;
        centroid.y += (y0 + y1) * a;
    }
    signedArea *= 0.5;
    centroid.x /= (6.0 * signedArea);
    centroid.y /= (6.0 * signedArea);

    // for (int i = 0; i < vertices.size(); ++i) {
    //     centroid.x = vertices[i].x;
    //     centroid.y = vertices[i].y;
    // }
    // centroid.x /= vertices.size();
    // centroid.y /= vertices.size();

    return centroid;
}

float GestureRecognition::calculateSD(std::vector<float> data)
{
    float sum = 0.f, mean = 0.f, standardDeviation = 0.f;
#pragma omp parallel for reduction(+ \
                                   : sum)
    for (int i = 0; i < data.size(); ++i) {
        sum += data[i];
    }
    mean = sum / data.size();
#pragma omp parallel for reduction(+ \
                                   : standardDeviation)
    for (int i = 0; i < data.size(); ++i) {
        standardDeviation += pow(data[i] - mean, 2);
    }
    return sqrt(standardDeviation / data.size());
}
