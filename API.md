# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`namespace `[`GestureRecognition`](#namespaceGestureRecognition) | This is a global namespace that wraps several classes and subclasses relating to gesture recognition. It wraps the classes based on input devices & gesture kernels
`class `[`Locator`](#classLocator) | 
`class `[`ofApp`](#classofApp) | 
`struct `[`GestureRecognition::Gesture::derivatives`](#structGestureRecognition_1_1Gesture_1_1derivatives) | A simple data structure to contain all relevant derivative values `[GestureRecognition::Gesture::derivatives::firstDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1a1864cd5eed4fe7d5fc73333ca77e8b48)` and `[GestureRecognition::Gesture::derivatives::secondDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1a22772e185f0f98dd00b1d4ac98185378)` store the velocity and acceleration respectively `[GestureRecognition::Gesture::derivatives::oldFirst](#structGestureRecognition_1_1Gesture_1_1derivatives_1a04892cfaa0a4276420966135c43b07b8)` and `[GestureRecognition::Gesture::derivatives::oldSecond](#structGestureRecognition_1_1Gesture_1_1derivatives_1ab22077f7e08e79f8c242586833380222)` store the previous values for velocity and acceleration respectively `[GestureRecognition::Gesture::derivatives::period](#structGestureRecognition_1_1Gesture_1_1derivatives_1a0fb595bafb19a5b8817773301015e2d8)` sets the measurement period for average calculation `[GestureRecognition::Gesture::derivatives::averageDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1ae05c09f3a4948ee2b336e68d36b5ea21)` and `[GestureRecognition::Gesture::derivatives::averageSecondDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1ac394e20a66c1ccc0fee3ec668c964d20)` store the average velocity and acceleration respectively
`struct `[`fingerStucture`](#structfingerStucture) | Data structure for joints of each finger.
`struct `[`handStructure`](#structhandStructure) | Data structure for a hand with individual fingers using `ofApp::fingerStructure`
`struct `[`leapHands`](#structleapHands) | Both hands together in a single structure.

# namespace `GestureRecognition` 

This is a global namespace that wraps several classes and subclasses relating to gesture recognition. It wraps the classes based on input devices & gesture kernels

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`enum `[`gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)            | Identifiers for basic gesture kernels.
`enum `[`Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)            | Identifiers for gestuare capable devices.
`public glm::vec2 `[`getCentroid`](#namespaceGestureRecognition_1ae978ae8b3bf793c6d1ae7d30dfbf6c91)`(vector< glm::vec2 > vertices)`            | Return centre of mass for the specified vertices. This is done by first forming a polygon sequentially from the vertices and then calculating its area. The normalized value is returned as centre or mass.
`public float `[`calculateSD`](#namespaceGestureRecognition_1a9b1df5babed63a4efaa03c9276917d5f)`(std::vector< float > data)`            | Return the standard deviation of the data collection. The mean value of the data set is calculated first. Each value subtracted with the mean is squared and summed. The normalized square-root of the sum is returned.
`class `[`GestureRecognition::Gesture`](#classGestureRecognition_1_1Gesture) | This class contains a set of tools to calculate motion derivatives for values of type `Template T`
`class `[`GestureRecognition::Identifier`](#classGestureRecognition_1_1Identifier) | This class accepts a device enum `[GestureRecognition::Device](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` and parses its output to query for gestures. The identified gestures are assiciated with the appropriate enums from `[GestureRecognition::gType](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)`
`struct `[`GestureRecognition::isGesture`](#structGestureRecognition_1_1isGesture) | A basic data structure to record the current value & type of gesture.

## Members

#### `enum `[`gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
SWIRL            | Circular or semi circular motion.
POINT            | One or more erect fingers.
CLAP            | Two hands with flat position in close proximity.
SPREAD            | Hands separted in space with fingers stretching out.
NONE            | No recognizable gesture.

Identifiers for basic gesture kernels.

#### `enum `[`Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
LEAP            | Leap Motion hand tracker.
KINECTV1            | Kinect V1 with tilt support.
KINECTV2            | Kinect V2 with body tracker for upto six people.
MOUSE            | Mouse, trackpad or Wacom Tablet.
CV            | Any device recognized by `openCV`
VOICE            | Audio input specific to voice.

Identifiers for gestuare capable devices.

#### `public glm::vec2 `[`getCentroid`](#namespaceGestureRecognition_1ae978ae8b3bf793c6d1ae7d30dfbf6c91)`(vector< glm::vec2 > vertices)` 

Return centre of mass for the specified vertices. This is done by first forming a polygon sequentially from the vertices and then calculating its area. The normalized value is returned as centre or mass.

#### `public float `[`calculateSD`](#namespaceGestureRecognition_1a9b1df5babed63a4efaa03c9276917d5f)`(std::vector< float > data)` 

Return the standard deviation of the data collection. The mean value of the data set is calculated first. Each value subtracted with the mean is squared and summed. The normalized square-root of the sum is returned.

# class `GestureRecognition::Gesture` 

This class contains a set of tools to calculate motion derivatives for values of type `Template T`

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`Gesture`](#classGestureRecognition_1_1Gesture_1acade1904260fb3dc7748a90ab4a79f9f)`(`[`isGesture`](#structGestureRecognition_1_1isGesture)` properties)` | 
`public  `[`Gesture`](#classGestureRecognition_1_1Gesture_1a2e5786eb79737b9b3c3c58f5572a6f25)`()` | 
`public  `[`~Gesture`](#classGestureRecognition_1_1Gesture_1a652fff7b3db3cd2a71141c3733f1b527)`()` | 
`public void `[`setup`](#classGestureRecognition_1_1Gesture_1a97c79b73cff7b0686a0d150e6d801ae9)`(`[`isGesture`](#structGestureRecognition_1_1isGesture)` properties)` | 
`public template<>`  <br/>`void `[`derive`](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)`(T value)` | Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value
`public void `[`derive`](#classGestureRecognition_1_1Gesture_1ac56f08d7d0eafd66a0dc13c9e1483376)`()` | Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value
`public void `[`average`](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`()` | Calculates and stores the average for the value set in `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)`. The time period for measurement is set from `GestureRecognition::Gesture::m_Deriv.period` which can be set from `[GestureRecognition::Gesture::setPeriod](#classGestureRecognition_1_1Gesture_1a0ec990c4296e6e40feae9c97219c5a44)`
`public float `[`velocity`](#classGestureRecognition_1_1Gesture_1a294a0fb8ec3069008de9ca3fb33b40fa)`()` | Returns the current velocity calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.
`public float `[`acceleartion`](#classGestureRecognition_1_1Gesture_1a6f05b44b338eff0645c5b892519dc3f4)`()` | Returns the current acceleartion calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.
`public float `[`oldValue`](#classGestureRecognition_1_1Gesture_1a9c33394aab34dd5f2d4e9ae59cb1ce12)`()` | Returns the previous velocity calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.
`public float `[`oldDt`](#classGestureRecognition_1_1Gesture_1ac9ca0cd435f2d5ea28b99cab2dfceb96)`()` | Returns the previous acceleration calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.
`public inline void `[`setPeriod`](#classGestureRecognition_1_1Gesture_1a0ec990c4296e6e40feae9c97219c5a44)`(unsigned int period)` | Sets the period for the calculation of average value by `[GestureRecognition::Gesture::average](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`
`public double `[`avgVelocity`](#classGestureRecognition_1_1Gesture_1aa4146eec0f4e49d7d43f7a585c71dea2)`()` | Returns the average velocity calculated by `[GestureRecognition::Gesture::average](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`
`public double `[`avgAccel`](#classGestureRecognition_1_1Gesture_1acd697dc75dc50d52a73cddf53b2b701b)`()` | Returns the average acceleartion calculated by `[GestureRecognition::Gesture::average](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`

## Members

#### `public  `[`Gesture`](#classGestureRecognition_1_1Gesture_1acade1904260fb3dc7748a90ab4a79f9f)`(`[`isGesture`](#structGestureRecognition_1_1isGesture)` properties)` 

#### `public  `[`Gesture`](#classGestureRecognition_1_1Gesture_1a2e5786eb79737b9b3c3c58f5572a6f25)`()` 

#### `public  `[`~Gesture`](#classGestureRecognition_1_1Gesture_1a652fff7b3db3cd2a71141c3733f1b527)`()` 

#### `public void `[`setup`](#classGestureRecognition_1_1Gesture_1a97c79b73cff7b0686a0d150e6d801ae9)`(`[`isGesture`](#structGestureRecognition_1_1isGesture)` properties)` 

#### `public template<>`  <br/>`void `[`derive`](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)`(T value)` 

Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value

#### `public void `[`derive`](#classGestureRecognition_1_1Gesture_1ac56f08d7d0eafd66a0dc13c9e1483376)`()` 

Generate and store the derivatives for the given value. This member function is ideally called inside a `while` loop. It stores the previous value and compares it against the current value

#### `public void `[`average`](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`()` 

Calculates and stores the average for the value set in `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)`. The time period for measurement is set from `GestureRecognition::Gesture::m_Deriv.period` which can be set from `[GestureRecognition::Gesture::setPeriod](#classGestureRecognition_1_1Gesture_1a0ec990c4296e6e40feae9c97219c5a44)`

#### `public float `[`velocity`](#classGestureRecognition_1_1Gesture_1a294a0fb8ec3069008de9ca3fb33b40fa)`()` 

Returns the current velocity calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.

#### `public float `[`acceleartion`](#classGestureRecognition_1_1Gesture_1a6f05b44b338eff0645c5b892519dc3f4)`()` 

Returns the current acceleartion calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.

#### `public float `[`oldValue`](#classGestureRecognition_1_1Gesture_1a9c33394aab34dd5f2d4e9ae59cb1ce12)`()` 

Returns the previous velocity calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.

#### `public float `[`oldDt`](#classGestureRecognition_1_1Gesture_1ac9ca0cd435f2d5ea28b99cab2dfceb96)`()` 

Returns the previous acceleration calculated from the most recent `[GestureRecognition::Gesture::derive](#classGestureRecognition_1_1Gesture_1a81669be01319d6c7ef04a78e6635edf8)` call.

#### `public inline void `[`setPeriod`](#classGestureRecognition_1_1Gesture_1a0ec990c4296e6e40feae9c97219c5a44)`(unsigned int period)` 

Sets the period for the calculation of average value by `[GestureRecognition::Gesture::average](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`

#### `public double `[`avgVelocity`](#classGestureRecognition_1_1Gesture_1aa4146eec0f4e49d7d43f7a585c71dea2)`()` 

Returns the average velocity calculated by `[GestureRecognition::Gesture::average](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`

#### `public double `[`avgAccel`](#classGestureRecognition_1_1Gesture_1acd697dc75dc50d52a73cddf53b2b701b)`()` 

Returns the average acceleartion calculated by `[GestureRecognition::Gesture::average](#classGestureRecognition_1_1Gesture_1a095895c4326f7687a628805f1ea40b38)`

# class `GestureRecognition::Identifier` 

This class accepts a device enum `[GestureRecognition::Device](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` and parses its output to query for gestures. The identified gestures are assiciated with the appropriate enums from `[GestureRecognition::gType](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)`

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`Identifier`](#classGestureRecognition_1_1Identifier_1a2dee7718260f94fb983c5919f9ed316f)`(`[`Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` dType)` | Default constructor.
`public  `[`Identifier`](#classGestureRecognition_1_1Identifier_1aa8387ca1ba4c6127bae09c59e1f26a9f)`()` | 
`public void `[`setDevice`](#classGestureRecognition_1_1Identifier_1ae644643fa6564530aa7184cddcb9cdac)`(`[`Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` dType)` | Set the device for its data be inspected. The value has to be of type `[GestureRecognition::Device](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)`
`public `[`gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)` `[`find`](#classGestureRecognition_1_1Identifier_1a9352ef82b2972e0aebee636fbae8d264)`(handData & data)` | The starting point of the algorithm. Pass the relevant data sets from the device. It iterates through possible gestures and returns when it finds one. Returns `GestureRecognition::gTyoe NONE` when unsuccessful.
`public bool `[`isPointed`](#classGestureRecognition_1_1Identifier_1ac7e22a25bb5f118e207b34f286d1ef31)`(handData data)` | Returns true or false for pointing gesture. If all the joints of any single finger form a semi-straight line, it returns true
`public bool `[`isRotation`](#classGestureRecognition_1_1Identifier_1a0cd8d7ce9bfa9ffec0493cbe555586ac)`(handData data)` | Returns true or false for rotation or swirling gesture. The dataset accumelated over a single second (60 frames, so 60 sets) are measured for center of mass using `[GestureRecognition::getCentroid](#namespaceGestureRecognition_1ae978ae8b3bf793c6d1ae7d30dfbf6c91)` which forms the center of the circle. The distance between the center and each vertex in the data set is measured and calculated for standard deviation using `[GestureRecognition::calculateSD](#namespaceGestureRecognition_1a9b1df5babed63a4efaa03c9276917d5f)`. Returns true when the SD is within a desired range.
`public bool `[`isClap`](#classGestureRecognition_1_1Identifier_1ad9d515be3a4d90d98f330a06757c43a0)`(handData data)` | Returns true or false for clapping gesture. Checks the proximity of individual joints of each finger. When they are close it increments a counter. If the counter is larger than twenty, or in other words if more than twenty of the fourty six joints are close enough then it returns true.

## Members

#### `public  `[`Identifier`](#classGestureRecognition_1_1Identifier_1a2dee7718260f94fb983c5919f9ed316f)`(`[`Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` dType)` 

Default constructor.

#### `public  `[`Identifier`](#classGestureRecognition_1_1Identifier_1aa8387ca1ba4c6127bae09c59e1f26a9f)`()` 

#### `public void `[`setDevice`](#classGestureRecognition_1_1Identifier_1ae644643fa6564530aa7184cddcb9cdac)`(`[`Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` dType)` 

Set the device for its data be inspected. The value has to be of type `[GestureRecognition::Device](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)`

#### `public `[`gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)` `[`find`](#classGestureRecognition_1_1Identifier_1a9352ef82b2972e0aebee636fbae8d264)`(handData & data)` 

The starting point of the algorithm. Pass the relevant data sets from the device. It iterates through possible gestures and returns when it finds one. Returns `GestureRecognition::gTyoe NONE` when unsuccessful.

#### `public bool `[`isPointed`](#classGestureRecognition_1_1Identifier_1ac7e22a25bb5f118e207b34f286d1ef31)`(handData data)` 

Returns true or false for pointing gesture. If all the joints of any single finger form a semi-straight line, it returns true

#### `public bool `[`isRotation`](#classGestureRecognition_1_1Identifier_1a0cd8d7ce9bfa9ffec0493cbe555586ac)`(handData data)` 

Returns true or false for rotation or swirling gesture. The dataset accumelated over a single second (60 frames, so 60 sets) are measured for center of mass using `[GestureRecognition::getCentroid](#namespaceGestureRecognition_1ae978ae8b3bf793c6d1ae7d30dfbf6c91)` which forms the center of the circle. The distance between the center and each vertex in the data set is measured and calculated for standard deviation using `[GestureRecognition::calculateSD](#namespaceGestureRecognition_1a9b1df5babed63a4efaa03c9276917d5f)`. Returns true when the SD is within a desired range.

#### `public bool `[`isClap`](#classGestureRecognition_1_1Identifier_1ad9d515be3a4d90d98f330a06757c43a0)`(handData data)` 

Returns true or false for clapping gesture. Checks the proximity of individual joints of each finger. When they are close it increments a counter. If the counter is larger than twenty, or in other words if more than twenty of the fourty six joints are close enough then it returns true.

# struct `GestureRecognition::isGesture` 

A basic data structure to record the current value & type of gesture.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public glm::vec3 `[`location`](#structGestureRecognition_1_1isGesture_1ae2b4dae527c0ffdb5f70e22a266f0d40) | The current co-ordinates to track gesture from.
`public `[`gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)` `[`type`](#structGestureRecognition_1_1isGesture_1a5070543b092e8b2cdbd58adfa9e8c3dc) | 

## Members

#### `public glm::vec3 `[`location`](#structGestureRecognition_1_1isGesture_1ae2b4dae527c0ffdb5f70e22a266f0d40) 

The current co-ordinates to track gesture from.

#### `public `[`gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)` `[`type`](#structGestureRecognition_1_1isGesture_1a5070543b092e8b2cdbd58adfa9e8c3dc) 

# class `Locator` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------

## Members

# class `ofApp` 

```
class ofApp
  : public ofBaseApp
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public ofxLeapMotion `[`leap`](#classofApp_1a789fbba38dcc59e25d179090f9d9c8e3) | Leap Motion API bindings.
`public vector< Hand > `[`hands`](#classofApp_1a320edd0c83778bc434fabec4eb56329a) | Hand structure and releveant data as exposed by Leap Motion API.
`public vector< int > `[`fingersFound`](#classofApp_1afe6f40185200448c94931e48a95143ce) | 
`public ofEasyCam `[`cam`](#classofApp_1a8fa3caf47e337c6cb16f362b415ba716) | A mouse controllable camera Matrix.
`public float `[`timer`](#classofApp_1a4fee6855812a6a407722961c57dc120b) | 
`public Poco::Stopwatch `[`watch`](#classofApp_1aabaf47fa539df91315fecddd771f0498) | 
`public int `[`counter`](#classofApp_1adea31a22af9cfa606efb97ff1dc29ee3) | 
`public ofImage `[`img`](#classofApp_1a3a87110a3314e9da3a0222f73431a625) | 
`public `[`fingerStucture`](#structfingerStucture)` `[`fingers`](#classofApp_1aa2982c530bf999482c1c015d02783ff7) | 
`public `[`handStructure`](#structhandStructure)` `[`structure`](#classofApp_1a2730e33cd33c239d6d7b210ba98a5acd) | 
`public `[`leapHands`](#structleapHands)` `[`Hands`](#classofApp_1a2b3dea1c98e855736cfad7b72186848f) | 
`public vector< vector< Hand > > `[`analyzer`](#classofApp_1a8570be66b23ca108b88536cf7f0a8eca) | 
`public `[`GestureRecognition::Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` `[`dev`](#classofApp_1af93cd270131206eee1b526843fb18175) | 
`public `[`GestureRecognition::Identifier`](#classGestureRecognition_1_1Identifier)` `[`ident`](#classofApp_1a8ce70fc612c8b85ca9e7728c277fdaf2) | 
`public `[`GestureRecognition::gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)` `[`type`](#classofApp_1a1c8cd786127a744e83a8dce087ec6e09) | 
`public float `[`radius`](#classofApp_1abccf0e70d1e0d79437d05bd67f9c3c16) | 
`public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` | 
`public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` | 
`public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` | Here is the reference implementation. Small balls (filled circles) are drawn on screen. Their location is controlled by the kind of identified gesture. They move ot different quadrants of the screen when the gesture changes.
`public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` | Destroy Leap Motion data on exit.

## Members

#### `public ofxLeapMotion `[`leap`](#classofApp_1a789fbba38dcc59e25d179090f9d9c8e3) 

Leap Motion API bindings.

#### `public vector< Hand > `[`hands`](#classofApp_1a320edd0c83778bc434fabec4eb56329a) 

Hand structure and releveant data as exposed by Leap Motion API.

#### `public vector< int > `[`fingersFound`](#classofApp_1afe6f40185200448c94931e48a95143ce) 

#### `public ofEasyCam `[`cam`](#classofApp_1a8fa3caf47e337c6cb16f362b415ba716) 

A mouse controllable camera Matrix.

#### `public float `[`timer`](#classofApp_1a4fee6855812a6a407722961c57dc120b) 

#### `public Poco::Stopwatch `[`watch`](#classofApp_1aabaf47fa539df91315fecddd771f0498) 

#### `public int `[`counter`](#classofApp_1adea31a22af9cfa606efb97ff1dc29ee3) 

#### `public ofImage `[`img`](#classofApp_1a3a87110a3314e9da3a0222f73431a625) 

#### `public `[`fingerStucture`](#structfingerStucture)` `[`fingers`](#classofApp_1aa2982c530bf999482c1c015d02783ff7) 

#### `public `[`handStructure`](#structhandStructure)` `[`structure`](#classofApp_1a2730e33cd33c239d6d7b210ba98a5acd) 

#### `public `[`leapHands`](#structleapHands)` `[`Hands`](#classofApp_1a2b3dea1c98e855736cfad7b72186848f) 

#### `public vector< vector< Hand > > `[`analyzer`](#classofApp_1a8570be66b23ca108b88536cf7f0a8eca) 

#### `public `[`GestureRecognition::Device`](#namespaceGestureRecognition_1a10d006d3766cdaaf183a30258d25c2b2)` `[`dev`](#classofApp_1af93cd270131206eee1b526843fb18175) 

#### `public `[`GestureRecognition::Identifier`](#classGestureRecognition_1_1Identifier)` `[`ident`](#classofApp_1a8ce70fc612c8b85ca9e7728c277fdaf2) 

#### `public `[`GestureRecognition::gType`](#namespaceGestureRecognition_1a6754c2caeef9b5ce84e15ca8acb67381)` `[`type`](#classofApp_1a1c8cd786127a744e83a8dce087ec6e09) 

#### `public float `[`radius`](#classofApp_1abccf0e70d1e0d79437d05bd67f9c3c16) 

#### `public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` 

#### `public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` 

#### `public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` 

Here is the reference implementation. Small balls (filled circles) are drawn on screen. Their location is controlled by the kind of identified gesture. They move ot different quadrants of the screen when the gesture changes.
A straight line is drawn based on the relationship between the left & right hand. Duration of their close proximity forms the coordinats of the start, The distance between the hands forms the xCoord of the end, with the magnitue of the vetor formed by the angle between both thumbs forms the yCoord of the end. Another circle is drawn based on the relationship formed between left thumb and right wrist

#### `public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` 

Destroy Leap Motion data on exit.

# struct `GestureRecognition::Gesture::derivatives` 

A simple data structure to contain all relevant derivative values `[GestureRecognition::Gesture::derivatives::firstDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1a1864cd5eed4fe7d5fc73333ca77e8b48)` and `[GestureRecognition::Gesture::derivatives::secondDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1a22772e185f0f98dd00b1d4ac98185378)` store the velocity and acceleration respectively `[GestureRecognition::Gesture::derivatives::oldFirst](#structGestureRecognition_1_1Gesture_1_1derivatives_1a04892cfaa0a4276420966135c43b07b8)` and `[GestureRecognition::Gesture::derivatives::oldSecond](#structGestureRecognition_1_1Gesture_1_1derivatives_1ab22077f7e08e79f8c242586833380222)` store the previous values for velocity and acceleration respectively `[GestureRecognition::Gesture::derivatives::period](#structGestureRecognition_1_1Gesture_1_1derivatives_1a0fb595bafb19a5b8817773301015e2d8)` sets the measurement period for average calculation `[GestureRecognition::Gesture::derivatives::averageDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1ae05c09f3a4948ee2b336e68d36b5ea21)` and `[GestureRecognition::Gesture::derivatives::averageSecondDt](#structGestureRecognition_1_1Gesture_1_1derivatives_1ac394e20a66c1ccc0fee3ec668c964d20)` store the average velocity and acceleration respectively

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public float `[`firstDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a1864cd5eed4fe7d5fc73333ca77e8b48) | Current velocity.
`public float `[`secondDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a22772e185f0f98dd00b1d4ac98185378) | Current acceleration.
`public float `[`oldFirst`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a04892cfaa0a4276420966135c43b07b8) | Previous velocity.
`public float `[`oldSecond`](#structGestureRecognition_1_1Gesture_1_1derivatives_1ab22077f7e08e79f8c242586833380222) | Previous acceleration.
`public unsigned int `[`period`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a0fb595bafb19a5b8817773301015e2d8) | Duration to meaure averages.
`public unsigned int `[`count`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a817e1fb3d86e20dac6e92434f212093a) | Current index in the duration cycle.
`public double `[`averageDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1ae05c09f3a4948ee2b336e68d36b5ea21) | Average velocity.
`public double `[`averageSecondDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1ac394e20a66c1ccc0fee3ec668c964d20) | Average acceleration.
`public inline  `[`derivatives`](#structGestureRecognition_1_1Gesture_1_1derivatives_1acdd7152e7fbb0ac0f42df28ffce240fa)`()` | Default initializer.
`public inline  `[`derivatives`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a5acdcef85df37ef33b06d57b49444d03)`(int value)` | Skip initialization to a specific value.
`public inline void `[`operator()`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a69a22e16b983c061e15b294534ab0754)`(int value)` | Copy constructor.

## Members

#### `public float `[`firstDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a1864cd5eed4fe7d5fc73333ca77e8b48) 

Current velocity.

#### `public float `[`secondDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a22772e185f0f98dd00b1d4ac98185378) 

Current acceleration.

#### `public float `[`oldFirst`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a04892cfaa0a4276420966135c43b07b8) 

Previous velocity.

#### `public float `[`oldSecond`](#structGestureRecognition_1_1Gesture_1_1derivatives_1ab22077f7e08e79f8c242586833380222) 

Previous acceleration.

#### `public unsigned int `[`period`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a0fb595bafb19a5b8817773301015e2d8) 

Duration to meaure averages.

#### `public unsigned int `[`count`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a817e1fb3d86e20dac6e92434f212093a) 

Current index in the duration cycle.

#### `public double `[`averageDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1ae05c09f3a4948ee2b336e68d36b5ea21) 

Average velocity.

#### `public double `[`averageSecondDt`](#structGestureRecognition_1_1Gesture_1_1derivatives_1ac394e20a66c1ccc0fee3ec668c964d20) 

Average acceleration.

#### `public inline  `[`derivatives`](#structGestureRecognition_1_1Gesture_1_1derivatives_1acdd7152e7fbb0ac0f42df28ffce240fa)`()` 

Default initializer.

#### `public inline  `[`derivatives`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a5acdcef85df37ef33b06d57b49444d03)`(int value)` 

Skip initialization to a specific value.

#### `public inline void `[`operator()`](#structGestureRecognition_1_1Gesture_1_1derivatives_1a69a22e16b983c061e15b294534ab0754)`(int value)` 

Copy constructor.

# struct `fingerStucture` 

Data structure for joints of each finger.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public glm::vec3 `[`dip`](#structfingerStucture_1ad18fc9e931b7201018e69d27e385fc5a) | 
`public glm::vec3 `[`mcp`](#structfingerStucture_1af5e81381e105c32d4f1fcf3fcc1852e1) | 
`public glm::vec3 `[`pip`](#structfingerStucture_1a15779cf9df41ad926d28c05529e7fc7c) | 
`public glm::vec3 `[`tip`](#structfingerStucture_1a626dca369d0e7648fa3050c0b28024c9) | 

## Members

#### `public glm::vec3 `[`dip`](#structfingerStucture_1ad18fc9e931b7201018e69d27e385fc5a) 

#### `public glm::vec3 `[`mcp`](#structfingerStucture_1af5e81381e105c32d4f1fcf3fcc1852e1) 

#### `public glm::vec3 `[`pip`](#structfingerStucture_1a15779cf9df41ad926d28c05529e7fc7c) 

#### `public glm::vec3 `[`tip`](#structfingerStucture_1a626dca369d0e7648fa3050c0b28024c9) 

# struct `handStructure` 

Data structure for a hand with individual fingers using `ofApp::fingerStructure`

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public glm::vec3 `[`handPos`](#structhandStructure_1add3baa292049ff87db27dd93cf512e76) | 
`public `[`fingerStucture`](#structfingerStucture)` `[`thumb`](#structhandStructure_1a3e279dd30fb3497a9301213ab4c5e193) | 
`public `[`fingerStucture`](#structfingerStucture)` `[`index`](#structhandStructure_1a1926b4d27cf558f669fdf61d65638ff0) | 
`public `[`fingerStucture`](#structfingerStucture)` `[`middle`](#structhandStructure_1a6bcd12ae096e2d342db11040f3ae45eb) | 
`public `[`fingerStucture`](#structfingerStucture)` `[`ring`](#structhandStructure_1aa064f2d781b353efee7d51b0b1a2b0b0) | 
`public `[`fingerStucture`](#structfingerStucture)` `[`little`](#structhandStructure_1a65ab6fe4c7f14da158dd95bf9baf240d) | 

## Members

#### `public glm::vec3 `[`handPos`](#structhandStructure_1add3baa292049ff87db27dd93cf512e76) 

#### `public `[`fingerStucture`](#structfingerStucture)` `[`thumb`](#structhandStructure_1a3e279dd30fb3497a9301213ab4c5e193) 

#### `public `[`fingerStucture`](#structfingerStucture)` `[`index`](#structhandStructure_1a1926b4d27cf558f669fdf61d65638ff0) 

#### `public `[`fingerStucture`](#structfingerStucture)` `[`middle`](#structhandStructure_1a6bcd12ae096e2d342db11040f3ae45eb) 

#### `public `[`fingerStucture`](#structfingerStucture)` `[`ring`](#structhandStructure_1aa064f2d781b353efee7d51b0b1a2b0b0) 

#### `public `[`fingerStucture`](#structfingerStucture)` `[`little`](#structhandStructure_1a65ab6fe4c7f14da158dd95bf9baf240d) 

# struct `leapHands` 

Both hands together in a single structure.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public `[`handStructure`](#structhandStructure)` `[`leftHand`](#structleapHands_1a06137a3456dcde71134a67b10e5b523b) | 
`public `[`handStructure`](#structhandStructure)` `[`rightHand`](#structleapHands_1a5b0eea5269972261358823c77b0ac8d4) | 

## Members

#### `public `[`handStructure`](#structhandStructure)` `[`leftHand`](#structleapHands_1a06137a3456dcde71134a67b10e5b523b) 

#### `public `[`handStructure`](#structhandStructure)` `[`rightHand`](#structleapHands_1a5b0eea5269972261358823c77b0ac8d4) 

Generated by [Moxygen](https://sourcey.com/moxygen)